# %% [markdown]
# # Realtime $R_t$ calculator for a Country 
# 
# based on data from github datasets

# %%
import pandas as pd
import numpy as np

from matplotlib import pyplot as plt
# from matplotlib.patches import Patch

# from scipy import stats as sps
# from scipy.interpolate import interp1d

from npp_covid.sup_Rt import highest_density_interval, prepare_cases, get_posteriors, plot_rt
from npp_covid.io.github_io import read_aggregated_from_github, get_subset, read_worldwide_from_gituhub
FILTERED_REGION_CODES = ['AS', 'GU', 'PR', 'VI', 'MP']

#%%
# PARAMETERS 
# We create an array for every possible value of Rt
R_T_MAX = 3
r_t_num = 250
#GAMMA = 1/7
posteriors_p = 0.90
cutoff = 5000 # this has an effect for high no of  cases
truncate_time_history=True
smooth_periods_count=15
smooth_stds=4

state_name= 'Worldwide'


# %%
dfw = read_worldwide_from_gituhub()

# %%
df_sub = dfw.drop(labels=['Recovered', 'Deaths', 'Increase rate','dt'], axis=1)
df_sub.columns = ['date', state_name]
df_sub.set_index('date', inplace=True)
# df_sub.tail()
# df_sub.head()


# %%
# if truncate_time_history:
#     df_sub = df_sub[-100:]
cases = df_sub[state_name].diff()

# %%
original, smoothed = prepare_cases(cases, is_cases_cumulative=False, cutoff=cutoff, 
    periods_count=smooth_periods_count, stds=smooth_stds)


# %%
# prepare_cases

# casesn = states.xs(state_name).rename(f"{state_name} cases")

original.plot(title=f" New Cases per Day",
               c='k',
               linestyle=':',
               alpha=.5,
               label='Actual',
               legend=True,
             figsize=(500/72, 300/72))

ax = smoothed.plot(label='Smoothed',
                   legend=True)

ax.get_figure().set_facecolor('w')


# %%
# get_posteriors
# Note that we're fixing sigma to a value just for the example
posteriors, log_likelihood = get_posteriors(smoothed, sigma=.15,
     R_T_MAX=R_T_MAX, r_t_num=r_t_num)

ax = posteriors.plot(title=f'{state_name} - Daily Posterior for $R_t$',
           legend=False, 
           lw=1,
           c='k',
           alpha=.3,
           xlim=(0.4,R_T_MAX))

ax.set_xlabel('$R_t$')


# %%
# Note that this takes a while to execute - it's not the most efficient algorithm
hdis = highest_density_interval(posteriors, p=posteriors_p )

most_likely = posteriors.idxmax().rename('ML')

# Look into why you shift -1
result = pd.concat([most_likely, hdis], axis=1)

result.tail()


# %%

plot_rt(result,  state_name, Rt_plot_start_date='2020-08-01')

plt.show()

# %%

# %%
