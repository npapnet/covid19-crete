
#%%
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error
import seaborn as sns

import  statsmodels.api as sm
from statsmodels.stats.outliers_influence import variance_inflation_factor


from npp_covid.io.heroku import hag_create_df
from npp_covid.misc.smooth_funcs import prepare_cases_gaussian
from npp_covid.mvr_analysis import ModelContainer, visualize_correlations
# %%





def plot_hag_date_index(col_str , r_ind=30, no_labels=5, title:str=None):
    """Wrapper function for plotting dates that do not overlap.

    Args:
        col_str ([type]): [description]
        r_ind (int, optional): [description]. Defaults to 30.
        no_labels (int, optional): [description]. Defaults to 5.
        title (str, optional): [description]. Defaults to None.
    """ 
    plt.figure()  
    tmp = hag_d[col_str]
    plt.plot(hag_d.index[-(r_ind):], tmp[-(r_ind):].values)
    plt.gca().xaxis.set_major_locator(mdates.DayLocator(range(0,r_ind+1,(r_ind+1)//no_labels)))
    if title is not None:
        plt.title(title)
    # plt.gca().xaxis.set_major_formatter(mdates.DateFormatter(""))
    plt.gcf().autofmt_xdate()


# %% create daily data from cumulative  
def generate_daily_values_dataset():
    """Generate daily values dataset from the original dataset

    Args:
        hagdf ([type]): [description]

    Raises:
        ValueError: [description]

    Returns:
        [type]: [description]
    """    
    # %% load data nad create df
    hagdf = hag_create_df()

    hag_d = hagdf.diff().dropna()
    hag_d = hag_d.astype('int32')
    hag_d.tail()

    # =================================
    #%% Cleanup unusual values for tests
    # there are some unusual high testing entries 
    #print( hag_d.index[hag_d.tests>30000] )  # prints indexes  
    hag_d.drop(hag_d.index[(hag_d.tests<0)|((hag_d.tests>30000) & (hag_d.index < '2020-10-01'))], inplace=True)
    hag_d.drop(hag_d.index[(hag_d.tests>1e6) & (hag_d.index < '2021-06-01')], inplace=True)

    # =================================
    #  Add calcuated values
    hag_d['confirmed_smoothed'] = prepare_cases_gaussian(hag_d.confirmed, cutoff=25, periods_count=14, stds=4)
    hag_d['tests_sm' ]= prepare_cases_gaussian(hag_d.tests, cutoff=25, periods_count=14, stds=4)

    # confirmed 
    hag_d['conf_per_1000t_act']=hag_d['confirmed']/hag_d['tests']*1000

    # smoothed
    hag_d['conf_per_1000t_sm']=hag_d['confirmed_smoothed']/hag_d['tests_sm']*1000
    
    hag_d.dropna(inplace=True)
    if hag_d.isnull().any().any():
        print( hag_d.isnull().any())
        raise ValueError("Need to remove NaN's")
    return hag_d

def merge_indxs(X, y):
    """function that takes two dataframes 

    Args:
        X (Data Frame): [description]
        y (DataSeries): [description]

    Returns:
        [type]: [description]
    """    # create index 
    indx = y.index.intersection(X.index)
    X = X.loc[indx, :]
    y = y.loc[indx]
    return X,y


def plots_exploration():

    #%% 
    ax = hag_d.loc[:,['confirmed', "confirmed_smoothed"]].plot()
    ax.set_ylabel('cases' )
    ax.set_title('Original and smoothed cases')
    #%% create confirmed per 1k tests ACTUAL
    plot_hag_date_index('conf_per_1000t_act', title='Actual Confirmed per 1000 tests' )
    plot_hag_date_index('conf_per_1000t_sm', r_ind=30, no_labels=5, title= 'Smoothed Confirmed per 1000 ')


    #%%
    ax =hag_d.plot.scatter(x="conf_per_1000t_act", y="conf_per_1000t_sm", s =10*pd.qcut(hag_d.tests,q=4,labels=False).values+1, alpha =0.3)
    # ax.set_aspect(aspect=1)
    ax.set_xlabel('Actual Confirmed cases per 1000 tests' )
    ax.set_ylabel('Confirmed cases per 1000 tests smoothed' )
    ax.set_title('comparison between actual and smoothed\n cases per 1000 tests (size ~ tests)')

    #%%

    #%%
    # markers = ['.','+','1','2']
    # mrks = [markers[i] for i in pd.qcut(hag_d.tests,q=4,labels=False).values]

    #%%
    ax =hag_d.plot.scatter(x="tests", y="conf_per_1000t_sm", color =pd.qcut(hag_d.tests,q=4,labels=False).values+1)
    ax.set_xlim([5000,30000])
    #%% compare confirmed per 1000 tests actual and smoothed


    hag_d.plot.scatter(x="conf_per_1000t_act", y="conf_per_1000t_sm")
    plt.title('scatter plot between actual and\n smoothed confirmed cases per 1000 tests')
    #%%
    hag_d.loc[:, ["conf_per_1000t_act", "conf_per_1000t_sm"]].plot()
    plt.title('Comparison between actual \nand smoothed confirmed cases per 1000 tests')
    #%%
    # hag_d.to_excel('testing_data.xlsx')
    plt.show()

if __name__ == '__main__':
    hag_d = generate_daily_values_dataset()
    #TODO: generate_daily_Values does too many things. (reads data, removes outliers (in a bad way), creates data)
    # this should be broken up, to accomodate for other countries. Especially the cleanup is problematic.  

    print(hag_d.tail())
    #%% [markdown]
    # # Plots
    # plots_exploration()
    # %%

    #%% Create features dataframe  and target data Frame

    def generate_features_df_v1(df, no_periods:int=7 ):
        """Model with confirmed and tests (initial model)

        Args:
            df ([type]): [description]
            no_periods (int, optional): [description]. Defaults to 7.

        Returns:
            [type]: [description]
        """    
        conf = df.confirmed
        tests = df.tests
        resdf= pd.DataFrame({'conf':conf, 'tests':tests})
        for i in range(1,no_periods):
            resdf['c{}'.format(i)] = conf.shift(i)
            resdf['t{}'.format(i)] = tests.shift(i)
        resdf.dropna(inplace=True)# this should be clear of NAs
        return resdf

    def generate_features_df_v2(df, no_periods:int=7 ):
        """This is a model with 1 over tests

        Args:
            df ([type]): [description]
            no_periods (int, optional): [description]. Defaults to 7.

        Returns:
            [type]: [description]
        """    
        conf = df.confirmed
        tests = df.tests
        resdf= pd.DataFrame({'conf':conf, 'tests':conf/tests})
        for i in range(1,no_periods):
            resdf['c{}'.format(i)] = conf.shift(i)
            resdf['t{}'.format(i)] =  conf.shift(i)/tests.shift(i)
        resdf.dropna(inplace=True)# this should be clear of NAs
        return resdf

    def generate_features_df_v3(df, no_periods:int=7 ):
        """This is a model with 1 over tests

        Args:
            df ([type]): [description]
            no_periods (int, optional): [description]. Defaults to 7.

        Returns:
            [type]: [description]
        """    
        conf = df.confirmed
        tests = df.tests
        resdf= pd.DataFrame({'conf':conf, 'tests':1/tests})
        for i in range(1,no_periods):
            resdf['c{}'.format(i)] = conf.shift(i)
            resdf['t{}'.format(i)] =  1/tests.shift(i)
        resdf.dropna(inplace=True)# this should be clear of NAs
        return resdf

    generate_features_df = generate_features_df_v2
    X = generate_features_df(df=hag_d,no_periods=7)
    target = hag_d.conf_per_1000t_sm 

    X, target = merge_indxs(X, target)

    # target.dropna(inplace=True)  # this should be clear of NAs
    if target.isnull().any():
        raise ValueError("At this point target should not have any NaN's")

    features = X # X.loc[target.index,:]

    # %%
    #%% Training and test dataset split
    # separate target from features

    X_train, X_test, Y_train, Y_test = train_test_split(features, target, 
        test_size=0.2)#, random_state=10)
    regr = LinearRegression().fit(X_train, Y_train)
    fitted_vals= regr.predict(X_train)

    # MSE = ((fitted_vals-target)**2).mean()[0]
    # RMSE = np.sqrt(MSE)
    # MSE = mean_squared_error(target, fitted_vals)
    # RMSE = np.sqrt(MSE)
    # print(MSE)
    # print(RMSE)
    print('R squeared for Xtrain {:.3f}'.format( regr.score(X_train, Y_train)))
    print('R squeared for Xtest {:.3f}'.format(regr.score(X_test, Y_test)))
    # %%
    regr.coef_
    regr.score(features,target)
    # %% save to disk pairplot
    plot_pairplot = False
    if plot_pairplot is True:
        sns_plot = sns.pairplot(features,  kind ='reg',plot_kws={'line_kws':{'color':'red'}, 'scatter_kws': {'alpha': 0.4}})
        sns_plot.savefig('features_pairplot.png')
    # %%
    # correlation of the entire data frame

    visualize_correlations(features)
    # %% How to Interpret Coefficients using p-Values and Statistical Significance
    X_incl_const = sm.add_constant(X_train)
    model = sm.OLS(Y_train, X_incl_const)
    results = model.fit()

    #results params
    results.pvalues
    pd.DataFrame({
        'coef':results.params,
        'p-value': np.round(results.pvalues,3)
    })
    # from statsmodels.stats.outliers_influence import variance_inflation_factor
    vifs = []
    for i in range(X_incl_const.shape[1]):
        vifs.append( variance_inflation_factor(exog=X_incl_const.values, exog_idx=i))

    dvifs = pd.DataFrame({'coef_name':X_incl_const.columns, 'vif':np.round(vifs, decimals=2)})
    dvifs
    # %% 
    # plotting the residue normalised wrt to target. 
    plt.figure()
    tmp = (results.resid/Y_train).sort_index()
    tmp.plot()
    print(tmp.describe())
    # %% Test for skewneess
    print(pd.DataFrame({'feat':features.skew(),'logfeat':np.log(features).skew()}))
    print(np.log(target).skew())

    # %%


    # %%
    mc = ModelContainer(features= X, target = target)
    mc.split_data(random_state=10)
    mc.prepare_model()
    mc.prepare_OLSmodel()
    print (mc.p_values())
    print (mc.variance_inflation_factors())
    print(mc.plot_residuals())
    # %%
    # %%
    res = mc.mc_score()
    ax= res.plot.scatter(x='train', y='test',  title='Comparison of train and tests scores\n for results of Monte Carlo simulations')
    ax.set_aspect('equal')
    plt.show()
# %% Asymptotic
    print('Asymptotic plots')
    asdf = pd.DataFrame({
        'conf':hag_d.confirmed,
        'tests':hag_d.tests,
        'pos_idx': hag_d.confirmed/hag_d.tests*1000,
        'sm_confirmed': hag_d.confirmed_smoothed,
        'sm_tests': hag_d.tests_sm,
        'sm_pos_idx': hag_d.conf_per_1000t_sm
        }    )
#%%
    asdf.eval('ratio_pos_idx = pos_idx/sm_pos_idx', inplace=True)
    asdf.eval('ratio_tests=tests/sm_tests', inplace=True)

    plt.figure(figsize=(6,9))
    plt.plot(asdf.ratio_tests, asdf.ratio_pos_idx,'.')
    plt.xlabel('test ratio $\\frac{actual}{smoothed}$')
    plt.ylabel('positivy index ratio $\\frac{actual}{smoothed}$')
    plt.xlim([0,2])
    plt.ylim([0,3])
    plt.grid()
# %%
    plt.figure(figsize=(6,9))
    plt.plot(asdf.tests, asdf.ratio_pos_idx,'.')
    plt.xlabel('test')
    plt.ylabel('positivy index ratio $\\frac{actual}{smoothed}$')
    # plt.xlim([0,2])
    # plt.ylim([0,3])
    plt.grid()
    
# %%
    plt.figure(figsize=(6,9))
    plt.plot(1/asdf.tests, asdf.ratio_pos_idx,'.')
    plt.xlabel('test')
    plt.ylabel('positivy index ratio $\\frac{actual}{smoothed}$')
    # plt.xlim([0,2])
    # plt.ylim([0,3])
    plt.grid()
    #%% lmplot 
    plt.figure(figsize=(6,9))
    lmplot2 = sns.lmplot(x='ratio_tests', y='ratio_pos_idx', data=asdf, order=2)
    lmplot2.set_axis_labels(x_var='test ratio $\\frac{actual}{smoothed}$',
        y_var='positivy index ratio $\\frac{actual}{smoothed}$')
    lmplot2.set_titles('lmplot for pos.index vs.test ratio')
    plt.grid()
    # %%    
    asdf['rec_tests'] = np.log(1/asdf.tests)
    res = sns.lmplot(x='rec_tests', y='ratio_pos_idx', data=asdf,order=2 )
    res.set_axis_labels(x_var='$\\left[\\ln(\\frac{1}{tests}\\right]$',
        y_var='positivity index ratio (actual wrt smoothed)')
    plt.title ('2nd order linear regression')
    plt.grid()


# %%
    plt.show()
# %%

