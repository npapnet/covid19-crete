
#%%
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np

import npp_covid.io.heroku as io_cd
# %%



def hag_create_df():
    df_cases =  io_cd.ha_get_all()
    df_tests= io_cd.hag_get_tests()

    df = df_cases.set_index('date').join(df_tests.set_index('date'))
    df['rapid-tests']=pd.to_numeric(df['rapid-tests'].fillna(0), downcast='integer')
    df['tests']=pd.to_numeric(df['tests'].fillna(0), downcast='integer')
    return df

def plot_hag_date_index(col_str , r_ind=30, no_labels=5, title:str=None):
    """Wrapper function for plotting dates that do not overlap.

    Args:
        col_str ([type]): [description]
        r_ind (int, optional): [description]. Defaults to 30.
        no_labels (int, optional): [description]. Defaults to 5.
        title (str, optional): [description]. Defaults to None.
    """ 
    plt.figure()  
    tmp = hag_d[col_str]
    plt.plot(hag_d.index[-(r_ind):], tmp[-(r_ind):].values)
    plt.gca().xaxis.set_major_locator(mdates.DayLocator(range(0,r_ind+1,(r_ind+1)//no_labels)))
    if title is not None:
        plt.title(title)
    # plt.gca().xaxis.set_major_formatter(mdates.DateFormatter(""))
    plt.gcf().autofmt_xdate()

def prepare_cases(new_cases , 
    cutoff=25, 
    periods_count=14, 
    stds=4):
    
    smoothed = new_cases.rolling(periods_count,
        win_type='gaussian',
        min_periods=1,
        center=True).mean(std=stds).round()
    
    idx_start = np.searchsorted(smoothed, cutoff)
    
    smoothed = smoothed.iloc[idx_start:]
    original = new_cases.loc[smoothed.index]
    
    return  smoothed


# %% load data nad create df
hagdf = hag_create_df()

# %% Clenaup 
hag_d = hagdf.diff().dropna()
hag_d = hag_d.astype('int32')
hag_d.tail()

hag_d.drop(hag_d.index[(hag_d.tests<0)|((hag_d.tests>30000) & (hag_d.index < '2020-10-01'))], inplace=True)
hag_d.drop(hag_d.index[(hag_d.tests>1e6) & (hag_d.index < '2021-06-01')], inplace=True)

# %%


hag_d['confirmed_smoothed'] = prepare_cases(hag_d.confirmed, cutoff=25, periods_count=14, stds=4)
hag_d['tests_sm' ]= prepare_cases(hag_d.tests, cutoff=25, periods_count=14, stds=4)

# confirmed 
hag_d['conf_per_1000t_act']=hag_d['confirmed']/hag_d['tests']*1000

# smoothed
hag_d['conf_per_1000t_sm']=hag_d['confirmed_smoothed']/hag_d['tests_sm']*1000

#%% [markdown]
# # Plots
# %% 
hag_d.loc[:,['confirmed', "confirmed_smoothed"]].plot()
# %% create confirmed per 1k tests ACTUAL
plot_hag_date_index('conf_per_1000t_act', title='Confirmed per 1000 tests' )
#%%
# hag_d['conf_per_1000t_sm'].iloc[-35:].plot()
plot_hag_date_index('conf_per_1000t_sm', r_ind=30, no_labels=5, title= 'Confirm per 1000 smoothed')
# %%
ax =hag_d.plot.scatter(x="conf_per_1000t_act", y="conf_per_1000t_sm", color =pd.qcut(hag_d.tests,q=4,labels=False).values+1)
# ax.set_aspect(aspect=1)

# %%

# %%
ax =hag_d.plot.scatter(x="tests", y="conf_per_1000t_sm", color =pd.qcut(hag_d.tests,q=4,labels=False).values+1)
ax.set_xlim([0,20000])
# %% compare confirmed per 1000 tests actual and smoothed


hag_d.plot.scatter(x="conf_per_1000t_act", y="conf_per_1000t_sm")
plt.title('scatter plot between actual and smoothed confirmed cases per 1000 tests')
# %%
hag_d.loc[:, ["conf_per_1000t_act", "conf_per_1000t_sm"]].plot()
plt.title('Comparison between actual and smoothed confirmed cases per 1000 tests')
# %%
hag_d.to_excel('testing_data.xlsx')
# %%
plt.show()

#%% Create hagdf 

def create_df_last(hagdf):
    conf = hagdf.confirmed.diff()
    resdf= pd.DataFrame({'conf':conf, 'c1':conf.shift(1)})
    for i in range(2,4):
        resdf['c{}'.format(i)] = conf.shift(i)
    return resdf

create_df_last(hagdf=hagdf)
#%%
# %%
