# # Realtime $R_t$ calculator for a Country 
# 
# based on data from github datasets

# %%
import pandas as pd
import numpy as np

from matplotlib import pyplot as plt
from matplotlib.patches import Patch

from scipy import stats as sps
from scipy.interpolate import interp1d

from IPython.display import clear_output

from npp_covid.sup_Rt import highest_density_interval, prepare_cases, get_posteriors, plot_rt , plot_rt_4oop, Rt_Calculator
from npp_covid.io.github_io import read_aggregated_from_github, get_subset


# PARAMETERS 
truncate_time_history=True

# We create an array for every possible value of Rt
cutoff = 20 # this has an effect for high no of  cases
smooth_periods_count=14
smooth_stds=4

R_T_MAX = 3
r_t_num = 50
#GAMMA = 1/7
posteriors_p = 0.90
posteriors_sigma = 0.15

state_name= 'Greece'
# state_name= 'US'

# state_name= 'Cuba'

plot_level = 0
RT_PLOT_START_DATE='2021-01-01'
RT_PLOT_END_DATE='2021-12-10'
# %%
df = read_aggregated_from_github()
# %% select data
df_sub = get_subset(df, Country=state_name)
df_sub = df_sub.iloc[:,[0,2]]
df_sub.columns = ['date', state_name]
df_sub.set_index('date', inplace=True)
print(df_sub.diff().tail())
cases_cum = df_sub[state_name]
cases_daily = cases_cum.diff()

# %% Main Processing
rt = Rt_Calculator(cases_daily, smoothed=False)
rt.smooth_clean(cutoff=cutoff, smooth_periods_count=smooth_periods_count, smooth_stds=smooth_stds)
rt.process(R_T_MAX=R_T_MAX, r_t_num=r_t_num, 
  GAMMA=1/7, 
  posteriors_p=posteriors_p, posteriors_sigma=posteriors_sigma)

# rt.result.plot()


# %%
if plot_level >=1:
  plt.figure()
  rt.raw.plot(title=f" New Cases per Day",
                c='k',
                linestyle=':',
                alpha=.5,
                label='Actual',
                legend=True,
              figsize=(500/72, 300/72))

  ax = rt.smoothed.plot(label='Smoothed',
                    legend=True)

  ax.get_figure().set_facecolor('w')
# %%


if plot_level>=2:

  ax = rt.posteriors.plot(title=f'{state_name} - Daily Posterior for $R_t$',
            legend=False, 
            lw=1,
            c='k',
            alpha=.3,
            xlim=(0.4,R_T_MAX))

  ax.set_xlabel('$R_t$')


# %%
# Note that this takes a while to execute - it's not the most efficient algorithm
print(rt.result.tail())


# state_name='Heraklion'

# plot_rt(rt.result, state_name= state_name, Rt_plot_start_date=RT_PLOT_START_DATE, Rt_plot_end_date=RT_PLOT_END_DATE, df_cases=rt.get_df_cases())


plot_rt_4oop(rt, state_name= state_name, 
  Rt_plot_start_date=RT_PLOT_START_DATE, Rt_plot_end_date=RT_PLOT_END_DATE, plot_cases=True)
plt.show()
# %%



