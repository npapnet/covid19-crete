#%% 
# this part of the code requires testing_Analysis.py executed first, in order to create the testing_data.xlsx file

#%%
import numpy as np

import scipy.stats as stats

import pandas as pd
import seaborn as sns



#%% Detect outliers
def detect_outliers_z_score(dfz, z_th:float=3):  
    print('outliers')
    dfout = dfz[(dfz.abs()>z_th).any(axis=1)]
    # display(dfout)
    return dfout

def clean_outliers_z_score(dfz, z_th:float=3):  
    print('clean data')
    dfclean = dfz.loc[(dfz.abs()<z_th).all(axis=1),:]
    # display(dfclean)
    return dfclean
#%% Feature selection
def feature_selection_with_corr(df):
    vars = df.columns
    k_tau = np.zeros((len(vars),len(vars)))
    k_tau_pval = np.zeros((len(vars),len(vars)))
    for i in range(len(vars)):
        for k in range(i, len(vars)):
            res = stats.kendalltau(df.iloc[:,i],df.iloc[:,k])
            k_tau[i,k]  = k_tau[k,i] = res.correlation
            k_tau_pval[i,k] = k_tau_pval[k,i]  = np.log(res.pvalue)
    return (k_tau, k_tau_pval)

# %% [markdown]
# # Prerequisities
# !conda install -c conda-forge altair vega_datasets
# !conda install seaborn
# %%



# %% [markdown]
# # Load Data Set

# %%
# df = data.iris().dropna().sample(frac=1)
sheet_name = 'Sheet1'

df = pd.read_excel('testing_data.xlsx',sheet_name=sheet_name,header=0)
dfdate = df['date']
# display(df)

df.drop(columns=['date', 'deaths', 'confirmed_smoothed', 'tests_sm' ], inplace=True)
df.drop(df[df['tests'] <= 0].index, inplace = True)
df.tail()

#%%
df.columns
# %% [markdown]
# # 1 - Descriptive statistics
# 
# %%
# απάντηση στο  1a μέχρι 1e
df.info()

df.describe()




# %% [markdown]
# # 2. Να εντοπιστούν ενδεχόμενες ελλιπείς τιμές (missing values) και να αφαιρεθούν
#%%
if df.isna().any().any():
    print("Υπάρχουν ΝΑ μέσα στο dataset")
    print("============== ΣΤΟΠ ----------")
    print(df[df.isna().any(axis=1)])
    # df.dropna()
    df.dropna(inplace=True) # dropping na lines
else:
    print("ΔΕΝ υπάρχουν ΝΑ μέσα στο dataset")

# %% [markdown]
# # 3. Να εντοπιστούν πιθανές ακραίες τιμές και να αφαιρεθούν χρησιμοποιώντας μια μεθοδολογία της επιλογής σας.
# 
# Για τον εντοπισμό των μέσων τιμών θα χρησιμοποιηθει το |z score|>3

#%%

# %% [markdown]
# # outliers

# %%
from matplotlib import pyplot as plt

plt.scatter(df['confirmed'],df['tests'])
#%%
# display(df)
# m = df.mean(axis=0)
# s = df.std(axis=0)

# dfz = (df-m)/s # be carefull! the output is also scaled here

dfz = (df- df.mean())/df.std()
dfz.tail()
# sns.boxplot(x=df['confirmed'])


# %%
print('plot with outlier')
g=sns.pairplot(data=df,diag_kind="kde",height=2.5)
#%%
print('plot z')
g1=sns.pairplot(data=dfz,diag_kind="kde",height=2.5)


# %%
# detect outliers
# 

def detect_outliers_z_score(dfz, z_th:float=3):  
    print('outliers')
    dfout = dfz[(dfz.abs()>z_th).any(axis=1)]
    # display(dfout)
    return dfout

def clean_outliers_z_score(dfz, z_th:float=3):  
    print('clean data')
    dfclean = dfz.loc[(dfz.abs()<z_th).all(axis=1),:]
    # display(dfclean)
    return dfclean

dfout = detect_outliers_z_score(dfz, z_th=3)
dfclean = clean_outliers_z_score(dfz, z_th=3)
print(dfout)
print(dfclean)

# th=3
# print('outliers')
# dfout = dfz[(dfz.abs()>th).any(axis=1)]
# # display(dfout)
# print(dfout)

# print('clean data')
# dfclean = dfz[(dfz.abs()<th).all(axis=1)]
# # display(dfclean)
# print(dfclean)


# %%
sns.pairplot(data=dfclean,diag_kind="kde",height=2.5)

plt.show()

# %% [markdown]
# # 4. Κανονικοποιήση 
# Τα δεδομένα που απομένουν να κανονικοποιηθούν στο διάστημα [0,1] Από την κανονικοποίηση να
# εξαιρείται η έξοδος 𝑦, η οποία δεν χρειάζεται να κανονικοποιηθεί.

# def normalise_data(df):
#     dfn = df.copy(deep = True)
#     dfn = (dfn- dfn.min())/(dfn.max()-dfn.min())
#     return dfn
# dfn = normalise_data(dfclean)
# dfn.y = dfclean.y
 

# alternatively look at sklearn.preprocessing

# %% [markdown]
# # 5 . scatter plot matrix
# Να γίνει scatter Plot, το οποίο να δείχνει τη σχέση μεταξύ κάθε ζεύγους μεταβλητών
# συμπεριλαμβανομένου και της εξόδου.

# ## Με Seaboarn 
# %%
#iris
df = dfn
sns.pairplot(data=df,diag_kind="kde",height=3)


# %% [markdown]
# # feature selection
# # %% [markdown]
# # ## pearson r

# # %% [markdown]
# # ### IQ test (data from wikipedia)

#%% 
def feature_selection_with_corr(df):
    vars = df.columns
    k_tau = np.zeros((len(vars),len(vars)))
    k_tau_pval = np.zeros((len(vars),len(vars)))
    for i in range(len(vars)):
        for k in range(i, len(vars)):
            res = stats.kendalltau(df.iloc[:,i],df.iloc[:,k])
            k_tau[i,k]  = k_tau[k,i] = res.correlation
            k_tau_pval[i,k] = k_tau_pval[k,i]  = np.log(res.pvalue)
    return (k_tau, k_tau_pval)

k_tau, k_tau_pval = feature_selection_with_corr(df)
sns.heatmap(k_tau,annot=True,robust=True)
plt.figure()
sns.heatmap(k_tau_pval,annot=True,robust=True)

#%%
sns.heatmap(df.corr(method='kendall'),annot=True,robust=True)
# %%


def calc_correlation_non_parametric(x, y):
    ''' This is probably kendall - need to make sure
    usage: 
    >>> print(calc_correlation_non_parametric(x=iq, y = h )) 
    '''
    lenX = len(x)
    total=0
    nc=0
    nd=0
    sum=0
    for i in range(len(x)):
        for j in range(i+1,len(x)):
            total+=1
            s = np.sign(x[i]-x[j])*np.sign(y[i]-y[j])
            sum+=s
            if s>0:
                nc+=s
            else:
                nd+=s
    # t probability for 0.5 and df=10 (requires import scipy.stats as stats)
    # stats.t.ppf(0.05, df=10)
    # 
    return {'total':total,'sum':sum,'nc':nc,'nd':nd,'tau':sum/total}

print(calc_correlation_non_parametric(x=iq, y = h ))



# # %% [markdown]
# # ## Random data sets
# # The following performs the following actions
# # 1. creates randomly dataset dfr
# # 1. calculates t
# # %%
# def compare_corr_coeff_for_random(N=1000):
#     dfr = pd.DataFrame()
#     dfr.at[:,'x']=np.random.normal(0,1,size=N)
#     dfr.at[:,'y']=np.random.normal(0,1,size=N)
#     # display(dfr)
#     sns.pairplot(data=dfr,vars=['x','y'],diag_kind="kde",kind='reg',height=2.5)

#     print('person',pearsonr(dfr.x,dfr.y) )
#     print('spearman',spearmanr(dfr.x,dfr.y) )
#     print('kendall',kendalltau(dfr.x,dfr.y,))
#     return dfr
# # compare_corr_coeff_for_random(N=1000)

# # %%


# # %% [markdown]
# # ## symmetric

# # %%

# def compare_corr_coeff_for_symmetric(N=100): 
#     dfr = pd.DataFrame()
#     dfr.at[:,'x']=np.linspace(0,np.pi,N+2)
#     dfr.at[:,'y']=np.sin(dfr.x)
#     # display(dfr)
#     sns.pairplot(data=dfr,vars=['x','y'],diag_kind="kde",kind='reg',height=2.5)

#     print('person',pearsonr(dfr.x,dfr.y) )
#     print('spearman',spearmanr(dfr.x,dfr.y) )
#     print('kendall',kendalltau(dfr.x,dfr.y))
#     return dfr
# dfr = compare_corr_coeff_for_symmetric(N=100)
# %% [markdown]
# ## t-Test

# # %%
# from scipy.stats import ttest_ind

# ttest_ind(iq,h,equal_var=False)


# %%
# ttest_ind(dfr.x,dfr.y,equal_var=False)


# %%



