
# %% [markdown]
# # Realtime $R_t$ calculator for Heraklion Crete

# %%
import pandas as pd
import numpy as np
import datetime

from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
from matplotlib.patches import Patch
from matplotlib import cm

from scipy import stats as sps
from scipy.interpolate import interp1d

from IPython.display import clear_output

from npp_covid.sup_Rt import highest_density_interval, prepare_cases, get_posteriors, plot_rt


# PARAMETERS 
# We create an array for every possible value of Rt
R_T_MAX = 6
r_t_num = 250
GAMMA = 1/7
posteriors_p = 0.90
cutoff = 1 # this has an effect for high no of  cases
truncate_time_history=True
smooth_periods_count=13
smooth_stds=4
plot_level = 1

RT_START_DATE = '2021-01-01'
RT_END_DATE = '2020-11-01'

state_name= 'Heraklion'
# state_name= 'Greece'


# %%
# url = 'https://covidtracking.com/api/v1/states/daily.csv'
# states = pd.read_csv(url,
#                      usecols=['date', 'state', 'positive'],
#                      parse_dates=['date'],
#                      index_col=['state', 'date'],
#                      squeeze=True).sort_index()

# states
# state_name = 'NY'
# cases = states.xs(state_name).rename(f"{state_name} cases")
# cases.head()


# %%

# fname = 'data/creteData.xlsx'
# fname = 'E:/home/share/nph-hmu/OneDrive - Hellenic Mediterranean University/Personal/XLS/Covid-Crete.xlsx'
# use the 'datafile.txt' to setup location fo covid-crete
with open('data/datafile.txt', 'r') as f:
    fname = f.readline()
print(fname)

df = pd.read_excel(fname, sheet_name=0, index_col='date' )
df = df.fillna(0)
df.columns = [ 'Greece', 'Heraklion', 'Lasithi', 'Rethimno', 'Chania','tests']
# convert_dict = {'Greece': int, 
#                 'Heraklion': int,
#                 'Lasithi': int,
#                 'Rethimno': int,
#                 'Chania': int
#                } 
# df = df.astype(convert_dict)

df.sort_index(inplace=True)

#%% The following 

try:
    removeToday = 1 if df.loc[pd.Timestamp.today().floor(freq='D'), state_name]==0 else 0
    ## remove days
    # day_to_keep =  np.sum(df.index<pd.Timestamp.today()- datetime.timedelta(removeToday))
    # df = df.iloc[:day_to_keep,:]
    ## The above two lines should be equivalent to this 
    df = df.loc[df.index<=pd.Timestamp.today()- datetime.timedelta(removeToday),:]
    print(removeToday)
except:
    print ("Heraklion does not have the latest data" )
print(df.tail())

# df.plot.scatter(x='Date', y='Heraklion')
df[state_name].plot()


# %%

cases=df[state_name]
cases.tail()

# %%
# casesn = states.xs(state_name).rename(f"{state_name} cases")

original, smoothed = prepare_cases(cases, is_cases_cumulative=False,  cutoff=cutoff,
    periods_count=smooth_periods_count, stds=smooth_stds)

dfCases = pd.DataFrame({'original':original, 'smoothed':smoothed})

if plot_level>=2:
    ax =  original.plot( title=f" New Cases per Day",
                c='k',
                linestyle=':',
                alpha=.5,
                label='Actual',
                legend=True,
                figsize=(500/72, 300/72))
    
    
    smoothed.plot(label='Smoothed',
                    legend=True, ax =ax )
    
    ax.get_figure().set_facecolor('w')
    ax.legend()

# %%
# get_posteriors
# Note that we're fixing sigma to a value just for the example
posteriors, log_likelihood = get_posteriors(smoothed, sigma=.25,
     R_T_MAX=R_T_MAX, r_t_num=r_t_num)


# %%
if plot_level>=2:
    fig = plt.figure()
    ax = posteriors.plot(title=f'{state_name} - Daily Posterior for $R_t$',
            legend=False, 
            lw=1,
            c='k',
            alpha=.3,
            xlim=(0.4,6))

    ax.set_xlabel('$R_t$')

# %%
# Note that this takes a while to execute - it's not the most efficient algorithm
hdis = highest_density_interval(posteriors, p=.9)

most_likely = posteriors.idxmax().rename('ML')

# Look into why you shift -1
result = pd.concat([most_likely, hdis], axis=1)

result.tail()


# %%
plot_rt(result=result,  state_name=state_name, df_cases=dfCases,  Rt_plot_start_date=RT_START_DATE)


if plot_level>=1:
    def plot3d_tests(df, n=30):
        fig = plt.figure(figsize=(14,14))
        ax = fig.add_subplot(111, projection='3d')

        # days = (df.index.map(lambda date:  (date-pd.Timestamp('2020-01-01 00:00:00'))) )/datetime.timedelta(1)
        days = (df.index.map(lambda date:  (date-df.index[0] )))/datetime.timedelta(1)

        dfc  =  df.loc[:,['Greece','tests']] 
        dfc['days'] = days

        ax.scatter(dfc.days[-n:], dfc.Greece.values[-n:]/dfc.tests.diff().values[-n:], dfc.tests.diff().values[-n:], 
        # ax.scatter(dfc.days[-n:], dfc.Greece.values[-n:], dfc.tests.diff().values[-n:], 
            s=1e3*(dfc.Greece.values[-n:]/dfc.tests.diff().values[-n:] ),
            c=1*(dfc.Greece.values[-n:]/dfc.tests.diff().values[-n:] ),
            cmap=cm.autumn
            )

        ax.set_xlabel('days')
        ax.set_ylabel('Positivity ratio []')
        ax.set_zlabel('tests')

    plot3d_tests(df,n=300//2)


# plot_rt2(result=result,  state_name=state_name, df_cases=dfCases,  Rt_plot_start_date=RT_START_DATE, Rt_plot_end_date=RT_END_DATE)

plt.show()

# %%
def plot_scatter(df, from_n=30, end_n=1):
    dfc  =  df.loc[:,['Greece','tests']] 
    plt.plot(dfc.tests.diff().values[-from_n:-end_n], 
        dfc.Greece.values[-from_n:-end_n]/dfc.tests.diff().values[-from_n:-end_n],'.')
    plt.xlabel('Number of PCR tests []')
    plt.ylabel('Positivity index [positive/tests]')
    
plot_scatter(df, from_n=90, end_n=60)
# %%

# %%
