# How to better estimate the confirmed cases in 1000 tests using AI. <!-- omit in TOC -->

- [1. Introduction](#1-introduction)
- [2. Methodology](#2-methodology)
  - [2.1. Raw Data](#21-raw-data)
  - [2.2. smoothing](#22-smoothing)
  - [2.3. outliers](#23-outliers)
  - [Machine learning](#machine-learning)
- [Results](#results)
  - [Confirmed cases](#confirmed-cases)
  - [Confirmed Cases per 1k tests](#confirmed-cases-per-1k-tests)


# 1. Introduction

During the covid19 pandemic, the variation in daily tests often gives wrong impressions on the actual state of the spread of the infection. 

For example in Greece the following tests were observed:

| Date | Cases | PCR Tests | Cases per 1k PCR tests |
| ---- | ---- | ---- |  ---- | 
| 2020-01-04 | 427  | 5048 | 132.03  | 
| 2020-01-05 | 927 | 20178 |  78.44 | 
| 2020-01-06 |  814 | 16405 | 58.64  | 
| 2020-01-07 |  510	| 3815 | 104.46 |

This short example shows that the number of tests have an effect of the percentage of the confirmed cases. Lower number of tests correlate well with higher number of percentages, while a higher number of tests is expected to yield a lower percentage. 

The number of confirmed cases is commonly used in the public reports. Another issue is that this number is commonly used to indicate the spread of the disease. However, that is usually misleading because generally fewer tests are expected to result in fewer cases (all things being equal).

Normalising the number of confirmed cases to the number of tests, also creates problems because of the dependence between tests and confirmed cases (this is not a case of random sampling, usually people submitted to a PCR test have either a symptom or an exposure). 

Therefore, it is  difficult to create a metric that accounts for the size. The aim here is to use machine learning to **obtain a (comparatively better) estimate of the confirmed cases with respect to 100 people**.  

# 2. Methodology

## 2.1. Raw Data

The data is collected through the publically available data at [covid-19-greece herokuapp](https://covid-19-greece.herokuapp.com/). The data sets that are downloaded are :

- confirmed cases/deaths
- PCR tests
- antigen tests

## 2.2. smoothing

A smoothing average is applied  on 
-  the confirmed cases 
-  the number of tests

The parameters that were selected after experimentation to smooth out the weekly variations were :  15 days, 4 stds.

## 2.3. outliers

There was one particular value in  March 22nd 2020 that the percentage increased from less than 100 $\left[\frac{\text{cases}}{\text{1000 tests}}\right]$. That is the only value worth considering removing.

## Machine learning

# Results

## Confirmed cases 

The following graph shows the smoothed data compared to the raw data for the confirmed cases.

![Comparison Between Confirmed Cases and SMoothed](./figures/ComparingConfirmedWithSmoothed.png)

## Confirmed Cases per 1k tests

The following graphs shows the confirmed cases per 1k PCR tests

![Comparison between actual and Smoothed for the Confirmed cases per 1k tests](./figures/Comparing_normalised_ConfirmedWithSmoothed.png)