
#%%
from datetime import time
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np

import npp_covid.heroku as io_cd
# %%



def hag_create_df():
    df_cases =  io_cd.ha_get_all()
    df_tests= io_cd.hag_get_tests()

    df = df_cases.set_index('date').join(df_tests.set_index('date'))
    df['rapid-tests']=pd.to_numeric(df['rapid-tests'].fillna(0), downcast='integer')
    df['tests']=pd.to_numeric(df['tests'].fillna(0), downcast='integer')
    return df

def plot_hag_date_index(col_str , r_ind=30, no_labels=5, title:str=None):
    """Wrapper function for plotting dates that do not overlap.

    Args:
        col_str ([type]): [description]
        r_ind (int, optional): [description]. Defaults to 30.
        no_labels (int, optional): [description]. Defaults to 5.
        title (str, optional): [description]. Defaults to None.
    """ 
    plt.figure()  
    tmp = hag_d[col_str]
    plt.plot(hag_d.index[-(r_ind):], tmp[-(r_ind):].values)
    plt.gca().xaxis.set_major_locator(mdates.DayLocator(range(0,r_ind+1,(r_ind+1)//no_labels)))
    if title is not None:
        plt.title(title)
    # plt.gca().xaxis.set_major_formatter(mdates.DateFormatter(""))
    plt.gcf().autofmt_xdate()

def prepare_cases(new_cases , 
    cutoff=25, 
    periods_count=14, 
    stds=4):
    
    smoothed = new_cases.rolling(periods_count,
        win_type='gaussian',
        min_periods=1,
        center=True).mean(std=stds).round()
    
    idx_start = np.searchsorted(smoothed, cutoff)
    
    smoothed = smoothed.iloc[idx_start:]
    original = new_cases.loc[smoothed.index]
    
    return  smoothed


# %% load data nad create df
hagdf = hag_create_df()

# %% Clenaup 
hag_d = hagdf.diff().dropna()
hag_d = hag_d.astype('int32')
hag_d.tail()
hag_d.plot(x='confirmed', y='deaths', kind='scatter', title='test')
# %%
def plot_conf_vs_death(hag_d):
    tindx = list(range(hag_d.shape[0]))
    fig, ax = plt.subplots(2, 1, figsize=(10, 6), sharex=True)
    ax[0].plot(tindx,hag_d['confirmed'], color='b', label='confirmed')
    ax[0].legend()
    ax[0].grid()
    ax[1].plot(tindx,hag_d['deaths'], color='r', label='deaths')
    ax[1].legend()
    ax[1].grid()
    plt.title('Cases and deaths starting {}'.format(hag_d.index[0]))
    plt.show()
plot_conf_vs_death(hag_d)
#%%
plot_conf_vs_death(hag_d[-240:].rolling(window=15, center=True).mean())

#%%

plot_conf_vs_death(hag_d[-240:-120].rolling(window=15, center=True).mean())

#%%

plot_conf_vs_death(hag_d[-120:].rolling(window=15, center=True).mean())

# %%
def calculate_time_lag(hag_d, time_lag_max=20):
    time_lags = list(range(time_lag_max))
    time_lag_corrs =[]
    for i in time_lags:
        
        time_lag_corrs.append(hag_d['confirmed'].corr(hag_d['deaths'].shift(-i)))
    return pd.DataFrame({'time_lags':time_lags, 'corr':time_lag_corrs})

res_time_lag = calculate_time_lag(hag_d=hag_d, time_lag_max=50)
plt.plot(res_time_lag['time_lags'], res_time_lag['corr'])
# %%
res_time_lag = calculate_time_lag(hag_d=hag_d[-100:], time_lag_max=50)
plt.plot(res_time_lag['time_lags'], res_time_lag['corr'])
#%%
res_time_lag = calculate_time_lag(hag_d=hag_d[:-100], time_lag_max=50)
plt.plot(res_time_lag['time_lags'], res_time_lag['corr'])
# %%
