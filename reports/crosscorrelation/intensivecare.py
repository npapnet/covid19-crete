#%%
import requests
import numpy as np
import pandas as pd

import npp_covid.heroku as io_cd
import matplotlib.pyplot as plt

#%%

#%%    
# response = requests.get("https://covid-19-greece.herokuapp.com/intensive-care")
# # response = requests.get("https://covid-19-greece.herokuapp.com/total-tests")

# dictr = response.json()
# df = pd.json_normalize (dictr['cases'])
 

#%%
dfic = io_cd.hag_get_intensive_care()
dfic.head()
# %%
df_cases =  io_cd.ha_get_all()
df_cases.head()


# %%
df = df_cases.set_index('date').join(dfic.set_index('date'))
df['confirmed'] = df['confirmed'].diff()
df['deaths'] = df['deaths'].diff()
df.dropna(inplace=True)
df.head()
df.tail()
# %%
def calculate_time_lag_cases_ic(hag_d, time_lag_max=20):
    time_lags = list(range(time_lag_max))
    time_lag_corrs =[]
    for i in time_lags:
        
        time_lag_corrs.append(hag_d['confirmed'].corr(hag_d['intensive_care'].shift(-i)))
    return pd.DataFrame({'time_lags':time_lags, 'corr':time_lag_corrs})


print (" Figure 1:  the correlation value against the  time lag shift ")
res_time_lag = calculate_time_lag_cases_ic(hag_d=df, time_lag_max=50)
plt.figure(1)
plt.plot(res_time_lag['time_lags'], res_time_lag['corr'])
# %%
def plot_conf_vs_ic(hag_d, figno=None):
    tindx = list(range(hag_d.shape[0]))
    fig, ax = plt.subplots(2, 1, figsize=(10, 6), sharex=True, num=figno)
    ax[0].plot(tindx,hag_d['confirmed'], color='b', label='confirmed')
    ax[0].legend()
    ax[0].grid()
    ax[1].plot(tindx,hag_d['intensive_care'], color='r', label='intensive_care')
    ax[1].legend()
    ax[1].grid()
    plt.title('Cases and IC  starting {}'.format(hag_d.index[0]))

print (" Figure 2:  the confirmed cases and the intensive care against the  time index ")
plot_conf_vs_ic(df, figno=2)
#%%
print (" Figure 3:  For the last 240 days the confirmed cases and the intensive care against the  time index ")
print ("         :  with a centered rolling mean of 15 ")
plot_conf_vs_ic(df[-240:].rolling(window=15, center=True).mean(),  figno=3)
# %%
print (" Figure 4:  For the last 240 days the confirmed cases and the intensive care against the  time index ")
print ("         :  with a centered rolling mean of 15 ")
plot_conf_vs_ic(df[-240:-150].rolling(window=15, center=True).mean(),  figno=4)
# %%
fig, ax = plt.subplots(1,1, num=5)
print (" Figure 5:  scatter plot of   confirmed cases and the intensive care ")
df.plot.scatter('confirmed', 'intensive_care',ax =ax)

# %%
#%%[markdown]
# ## Plot sum of last n days 
#
# %%
nshift = 20
df['confirm_cumsum'] =df.confirmed.rolling(nshift, center=False).sum()
# %%
print (f" Figure 6:  scatter plot of cumulative confirmed cases (shifted by {nshift}) for the past {nshift} days and the intensive care ")
fig, axs = plt.subplots(1,1, figsize=(12,8), num =6)
plt.plot(df.confirm_cumsum, df.intensive_care, lw=0, marker='.')
plt.xlabel(f'Cumulative confirmed for past {nshift} days []')
plt.ylabel(f'intensive care []')

#%%
t7= f" Figure 7:  scatter plot of cumulative confirmed cases (shifted by {nshift}) for all days and the intensive care against time"
print (t7)
fig, axs = plt.subplots(2,1, figsize=(12,8),  num =7)
axs[0].plot(df.index, df.confirm_cumsum,lw=0, marker='.')
axs[1].plot(df.index, df.intensive_care,lw=0, marker='.')
plt.title(t7)
# %%
#%%
t8= f" Figure 8:  scatter plot of cumulative confirmed cases (shifted by {nshift}) for all days and the deaths against time"
print (t8)
fig, axs = plt.subplots(1,1, figsize=(12,8),  num =8)
axs.plot(df.confirm_cumsum, df.deaths, lw=0, marker='.')
axs.set_xlabel(f'Cumulative confirmed for past {nshift} days []')
axs.set_ylabel(f'deaths []')
plt.title(t8)
# %% [markdown]
# ## Find best correlation for different shifts

# %%
def calculate_time_lag_cases_cumsum_ic(hag_d, time_lag_max=20):
    time_lags = list(range(time_lag_max))
    time_lag_corrs =[]
    for i in time_lags:
        time_lag_corrs.append(hag_d['intensive_care'].corr(hag_d.confirmed.rolling(i, center=False).sum()))
    return pd.DataFrame({'time_lags':time_lags, 'corr':time_lag_corrs})

res_time_lag_cs = calculate_time_lag_cases_cumsum_ic(hag_d=df[:-100], time_lag_max=60)

t9= f" Figure 9:  scatter plot correlation of intensive care vs confirmed cases shifted time delays "
print (t9)
fig, axs = plt.subplots(1,1, figsize=(12,8), num =9)
axs.plot(res_time_lag_cs['time_lags'], res_time_lag_cs['corr'])
plt.show()

# %%
res_time_lag_cs['corr'].idxmax()
# %%
df.confirmed.plot()
# %%
plt.show()