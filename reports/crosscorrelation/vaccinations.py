#%%
import matplotlib
import requests
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import npp_covid.heroku as io_cd

from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
#%% Load data
GREECE_POPULATION  = 1e7
VACC_PERC_STR = 'vacc_perc'

dfvac=io_cd.hag_get_vaccinations()
dfvac.index.rename('date',inplace=True)
dfvac.drop(columns = ['totaldose1','totaldose2', 'totalvaccinations'],inplace=True)
#%%  sandbox to generate hag_get_vaccinations()
# response = requests.get("https://covid-19-greece.herokuapp.com/vaccinations-per-region-history")
# # response = requests.get("https://covid-19-greece.herokuapp.com/total-tests")

# dictr = response.json()
# df = pd.json_normalize (dictr['vaccinations-history']) 
# dn = df.drop(columns = ['area_en', 'area_gr', 'dailydose1', 'dailydose2', 'daydiff', 'daytotal']) 
# dfvac = dn.groupby('referencedate').sum()
#%% various other
# df['area_gr'].unique().shape
# df.columns

#%%
dfvac.plot()

dfvac['totaldistinctpersons'].plot()
# %% [markdown]
# The following code is to see what 


#%%
dfic = io_cd.hag_get_intensive_care()
dfic.head()
# %%
df_cases =  io_cd.ha_get_all()
df_cases.head()


# %%
df = df_cases.set_index('date').join(dfic.set_index('date'))
df['confirmed'] = df['confirmed'].diff()
df['deaths'] = df['deaths'].diff()
df.dropna(inplace=True)
df.head()
df.tail()
# %%
df0 = df.join(dfvac)
# %% there are some gaps in the vaccination
def prepare_dataframe(df0, country_pop = GREECE_POPULATION):
    """Prepares a dataframe for the vaccination effect with the data

    operations
    - fills vaccination data with nearest 
    - fills na values in vaccination data with zero
    - creates conf, that contains a smoothed version of confirmed. 

    Args:
        df0 ([type]): dataframe with columns ['confirmed',	'deaths',	'intensive_care', 'totaldistinctpersons']
        country_pop ([type], optional): [description]. Defaults to GREECE_POPULATION.

    Returns:
        df_prep: [description]
    """    
    tmp = df0['totaldistinctpersons'].reset_index().interpolate(method='nearest')
    tmp.fillna(0,inplace=True)
    tmp.set_index('date', inplace=True)

    df_prep = df0.copy()
    df_prep['totaldistinctpersons'] = tmp['totaldistinctpersons']
    df_prep[VACC_PERC_STR] = tmp['totaldistinctpersons']/country_pop
    # smooth confirmed
    df_prep['conf']= df0['confirmed'].rolling(window=15, center=True).mean()
    df_prep.dropna(inplace=True)

    return df_prep
df0 = prepare_dataframe(df0)
# %%
df0.plot()
# %%
def plot_conf_ic_vac(hag_d):
    tindx = list(range(hag_d.shape[0]))
    fig, ax = plt.subplots(3, 1, figsize=(10, 6), sharex=True)
    ax[0].plot(tindx,hag_d['confirmed'], color='b', label='confirmed')
    ax[0].legend()
    ax[0].grid()
    ax[1].plot(tindx,hag_d['intensive_care'], color='r', label='intensive_care')
    ax[1].legend()
    ax[1].grid()
    ax[2].plot(tindx,hag_d[VACC_PERC_STR], color='r', label='vac')
    ax[2].legend()
    ax[2].grid()
    plt.title('Cases and IC  starting {}'.format(hag_d.index[0]))
    plt.show()
plot_conf_ic_vac(df0)
# %%
# %%
plt.scatter(df0[VACC_PERC_STR], (df0['deaths'].diff() /df0['conf'].diff()).fillna(0))

# %%
n_shift = 20
plt.scatter(df0[VACC_PERC_STR], (df0['deaths'].shift(-n_shift )/df0['conf']))

# %%
# @interact(x=(0.0,20.0,0.5))
@interact(x=widgets.IntSlider(min=0, max=30, step=1, value=10))
def h(n_shift = 10):
    plt.scatter(df0[VACC_PERC_STR], (df0['deaths'].shift(-n_shift )/df0['conf']))
    plt.ylim([0,.1])
    plt.ylabel('deaths_shifted /confirmed')
    plt.xlabel('vaccinated perc')
# %%

# %% [markdown]
# # 3D plot 
# the following section creates a 3d plot from the df0
#%%
%matplotlib qt
# %matplotlib inline
#%%

def plot3d_deaths_conf_vacc(df0, n_shift=20):
    ''' plots 3d graph with
    - x: deaths shifted /confirmed ratio
    - y: 'Vaccination percentage
    - z: confirmed cases

    requiqes `% matplotlib qt` for plotting through Visual studio
    '''
    from mpl_toolkits import mplot3d

    fig = plt.figure()
    ax = plt.axes(projection='3d')
    # Data for three-dimensional scattered points
    xdata = (df0['deaths'].shift(-n_shift )/df0['conf'])
    ydata = df0[VACC_PERC_STR]
    zdata = df0['conf']
    # ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Greens')
    ax.scatter3D(xdata, ydata, zdata, cmap='Greens')
    ax.set_xlabel('deaths/confirmed ratio')
    ax.set_xlim([0,0.05])
    ax.set_zlabel('conf')
    ax.set_ylabel('Vaccination')

    plt.show()

# %% [markdown]
# # Effect of the vaccination rate on the relationship between deaths and confirmed cases
# 
# The following graph  provide a scatter plot with confirmed and deaths 
# with different markers for different ranges of points
#  
%matplotlib inline
#%% sandbox
# n_shift=20
# # df0['markers']=pd.cut(df0['totaldistinctpersons'], bins=5, labels=False).values
# df0['markers']=pd.cut(df0[VACC_PERC_STR], bins=5)
# df0['deaths_shift'] = df0['deaths'].shift(-n_shift )
# # df0.plot.scatter(x = 'conf', y='deaths')
# df0.plot.scatter(x = 'conf', y='deaths_shift')
#
# plt.figure(figsize=(12,8))
# sns.scatterplot(data=df0, x='conf', y='deaths_shift', style="markers", hue='markers' ,s=50)
# %%
def plot_deaths_conf_wrt_vaccRate(df0,   shift =20, figsize=(12,8)):
    """fucntion that plots the data 
    - x: confirmed cases
    - y: deaths shifted /confirmed ratio
    - markers and hue depict: 'Vaccination percentage

    Args:
        df0 ([type]): pd.DataFrame
        shift (int, optional): [description]. Defaults to 20.
        figsize (tuple, optional): [description]. Defaults to (12,8).
    """    
    df0['markers']=pd.cut(df0['totaldistinctpersons'], bins=5)
    df0['deaths_shift'] = df0['deaths'].shift(-n_shift )
    plt.figure(figsize=figsize)
    sns.scatterplot(data=df0, x='conf', y='deaths_shift', style="markers", hue='markers')

plot_deaths_conf_wrt_vaccRate(df0, shift=21)
# %%
