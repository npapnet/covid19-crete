#%%
import matplotlib
import requests
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import npp_covid.owid_gh as io_owid
import npp_covid.vacc_eff.vacc_effect as cv_vee

from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import pathlib
#%%
# %%
file = pathlib.Path(io_owid.LOCAL_OWID_FILE)
if file.exists():
    df = pd.read_csv(file) 
    print("File read locally")
else:
    df = io_owid.get_owid_data_file(save_locally=True)
    print("File downloaded from internet")

# %%
df.columns
# keep_columns = [ 'iso_code', 'continent', 'location', 'date',
#         'total_cases', 'new_cases', 'new_cases_smoothed', 
#         'total_deaths','new_deaths', 'new_deaths_smoothed', 
#         'total_cases_per_million', 'new_cases_per_million', 'new_cases_smoothed_per_million',
#         'total_deaths_per_million', 'new_deaths_per_million', 'new_deaths_smoothed_per_million', 
#         'reproduction_rate', 
#         'icu_patients', 'icu_patients_per_million', 
#         'hosp_patients', 'hosp_patients_per_million', 
#         'weekly_icu_admissions', 'weekly_icu_admissions_per_million', 
#         'weekly_hosp_admissions', 'weekly_hosp_admissions_per_million', 
#         'new_tests', 'total_tests',
#         'total_tests_per_thousand', 'new_tests_per_thousand',
#         'new_tests_smoothed', 'new_tests_smoothed_per_thousand',
#         'positive_rate', 'tests_per_case', 'tests_units', 
#         'total_vaccinations', 'people_vaccinated', 'people_fully_vaccinated', 
#         'new_vaccinations', 'new_vaccinations_smoothed', 'total_vaccinations_per_hundred',
#         'people_vaccinated_per_hundred', 'people_fully_vaccinated_per_hundred',
#         'new_vaccinations_smoothed_per_million', 
#         'stringency_index',
#         'population', 'population_density', 
#         'median_age', 'aged_65_older', 'aged_70_older', 
#         'gdp_per_capita', 'extreme_poverty',
#         'cardiovasc_death_rate', 'diabetes_prevalence', 
#         'female_smokers', 'male_smokers', 
#         'handwashing_facilities', 'hospital_beds_per_thousand',
#         'life_expectancy', 'human_development_index', 'excess_mortality' ]

keep_columns = [ 'iso_code',  'date',
        # 'new_cases',
        'new_cases_per_million',
        'new_deaths',
        # 'icu_patients', 
        'people_vaccinated',
        'population' ]
ISO_CODES = df.iso_code.unique()
POPULATIONS = df.groupby(by='iso_code')['population'].mean()
# %%
df0 = df.loc[:,keep_columns]
df0.rename(columns={
        'new_cases':'confirmed',
        'new_cases_per_million':'confirmed',
        'new_deaths':'deaths',
        'icu_patients': 'intensive_care', 
        'people_vaccinated': 'totaldistinctpersons' }, inplace=True)
# %%
df0.set_index('date', inplace=True)
# %%
# dftmp= df0[df0.iso_code=='GRC'].copy()
# dftmp.intensive_care = 0
# dfpt = cv_vee.prepare_dataframe(df0=dftmp)
# cv_vee.plot_deaths_conf_wrt_vaccRate(df0=dfpt, n_shift=20)
# %% [markdown]
# # the following section does not work 
# 
# there is  a problem with the creation of the big dataframe. 
#  
# Consider looking at suggestions [here](https://stackoverflow.com/questions/13784192/creating-an-empty-pandas-dataframe-then-filling-it)
# for how to make the dfpt
# probably the best way is with the creation of lists. 
# %%

def process_isoc_code(df0, country_iso_code:str):
    dftmp= df0[df0.iso_code == country_iso_code].copy()
    # perform tests
    if np.isnan(dftmp.totaldistinctpersons[-1]):
        print('NOT processing: {}'.format(country_iso_code))
        return None
    else:
        print('processed:      {}'.format(country_iso_code))
        return cv_vee.prepare_dataframe(df0=dftmp, country_pop=POPULATIONS[country_iso_code])
#%%

countries = []
nshift = 21
for country_iso_code in ISO_CODES:
    atemp = process_isoc_code(df0, country_iso_code=country_iso_code)
    if atemp is not None:
        atemp['deaths_shift'] = atemp['deaths'].shift(-nshift)
        countries.append(atemp)
total = pd.concat(countries)
# %%
total.dropna(inplace=True)
total
# %%

# %%
dsrange = lambda df: (df.min(), df.max())

def plot_deaths_conf_wrt_vaccRate(df0, bins=5,  figsize=(12,8), ylim=None, xlim=None, **plt_kwargs):
    """fucntion that plots the data 
    - x: confirmed cases
    - y: deaths shifted /confirmed ratio
    - markers and hue depict: 'Vaccination percentage

    Args:
        df0 ([type]): pd.DataFrame
        shift (int, optional): [description]. Defaults to 20.
        figsize (tuple, optional): [description]. Defaults to (12,8).
    """    
    xlims = dsrange(df0.conf) if xlim is None else xlim
    ylims = dsrange(df0.deaths_shift) if ylim is None else ylim
    df0.loc[:,'markers']=pd.cut(df0.loc[:,'vacc_perc'], bins=bins)
    m_size  = df0['vacc_perc'].values/df0['vacc_perc'].max() *10
    plt.figure(figsize=figsize)
    sns.scatterplot(data=df0, x='conf', y='deaths_shift', style="markers", hue='markers', size=m_size,   **plt_kwargs)
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    plt.xlim(xlims)
    plt.ylim(ylims)
#%%
plot_deaths_conf_wrt_vaccRate(total[total.vacc_perc>0.1], bins=7, xlim = [1e1,2e3], ylim = [1e1,2e4], alpha=0.8,sizes=(3,100))
plot_deaths_conf_wrt_vaccRate(total[total.vacc_perc<0.1], bins=7, xlim = [1e1,2e3], ylim = [1e1,2e4], alpha=0.8,sizes=(3,100))

# %%
plot_deaths_conf_wrt_vaccRate(total[total.vacc_perc>0.1], bins=7, xlim=[1e2, 1e6], ylim=[1, 2e4],alpha=0.8,sizes=(3,100))
plot_deaths_conf_wrt_vaccRate(total[total.vacc_perc<0.1], bins=7, xlim=[1e2, 1e6], ylim=[1, 2e4],alpha=0.8,sizes=(3,100))
# %%

def relplot_deaths_conf_wrt_vaccRate(df0, bins=5,  figsize=(12,8), ylim=None, xlim=None, **plt_kwargs):
    """fucntion that plots the data 
    - x: confirmed cases
    - y: deaths shifted /confirmed ratio
    - markers and hue depict: 'Vaccination percentage

    Args:
        df0 ([type]): pd.DataFrame
        shift (int, optional): [description]. Defaults to 20.
        figsize (tuple, optional): [description]. Defaults to (12,8).
    """    
    xlims = dsrange(df0.conf) if xlim is None else xlim
    ylims = dsrange(df0.deaths_shift) if ylim is None else ylim
    df0.loc[:,'markers']=pd.cut(df0.loc[:,'vacc_perc'], bins=bins)
    m_size  = df0['vacc_perc'].values/df0['vacc_perc'].max() *10
    plt.figure(figsize=figsize)
    # sns.scatterplot(data=df0, x='conf', y='deaths_shift', style="markers", hue='markers', size=m_size,   **plt_kwargs)
    sns.relplot(
        data=df0, x="conf", y="deaths_shift",
        col="vacc_perc", hue="markers", style="markers",
        kind="scatter"
    )
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    plt.xlim(xlims)
    plt.ylim(ylims)
relplot_deaths_conf_wrt_vaccRate(total, bins=7, xlim=[1e2, 1e6], ylim=[1, 2e4],alpha=0.8,sizes=(3,100))
# %%
bins, figsize =4 , (12,8)
df0 = total.copy()
xlims = dsrange(df0.conf) 
ylims = dsrange(df0.deaths_shift) 
df0 = df0[df0.vacc_perc<0.4]
df0.loc[:,'markers']=pd.cut(df0.loc[:,'vacc_perc'], bins=bins)
m_size  = df0['vacc_perc'].values/df0['vacc_perc'].max() *10

# sns.scatterplot(data=df0, x='conf', y='deaths_shift', style="markers", hue='markers', size=m_size,   **plt_kwargs)
#%%
plt.figure(figsize=figsize)
sns.relplot(
    data=df0, x="conf", y="deaths_shift",
    col="markers", hue="markers", style="markers",
    kind="scatter"
)
plt.xscale('log')
plt.yscale('log')
plt.xlim([1e3,1e6])
plt.ylim([1e0,1e4])
plt.show()
# %%

# %%
